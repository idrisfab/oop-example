package com.fabiyi.musiclib;

import java.util.ArrayList;
import java.util.Scanner;

import com.fabiyi.tools.Date;

public class FabMusicPlayer 
{
	private Library playerLib;
	
	public FabMusicPlayer() 
	{
		playerLib = new Library();
		displayMenu();
	}  	
	
	public void displayMenu()
	{
		System.out.println("***** Welcome to the Fab Music Library *****");
		System.out.println("*                                          *");
		System.out.println("*     Press 1 to add a song                *");
		System.out.println("*     Press 2 to view all songs            *");
		System.out.println("*     Press 3 find  a song by ID           *");
		System.out.println("*     Press 4 find  a song by name         *");
		System.out.println("*     Press 5 for help                     *");
		System.out.println("*                                          *");
		System.out.println("********************************************");
		
		Scanner scan = new Scanner(System.in);
		
		while (scan.hasNext()) 
		{
			String string = (String) scan.next();
			if(string.equalsIgnoreCase(1+""))
			{
				addASong();
			}
			if(string.equalsIgnoreCase(2+""))
			{
				displayAllLibrary(playerLib);
			}
			
			if(string.equalsIgnoreCase(3+""))
			{
				Scanner scan2 = new Scanner(System.in);		
				try {
					System.out.println("Specify the ID you are looking for ");
					int num = scan2.nextInt();

					displaySongInfo(playerLib, num);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("Enter only a number");
					e.printStackTrace();
				}
				scan2.close();

			}
			if(string.equalsIgnoreCase(4+""))
			{
				Scanner scan3 = new Scanner(System.in);		
				try 
				{
					System.out.println("Specify the name you are looking for ");
					String songName = scan3.next();

					displaySongInfo(playerLib, songName);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("Enter only a number");
					e.printStackTrace();
				}
				scan3.close();
			}
		}
	}
	
	public void addASong()
	{
		System.out.println("********************************************");
		System.out.println("*          Add a song selected             *");
		System.out.println("********************************************");
		
		// create a song from 
		Song newSong = new Song();
		
		// set the ID for the track
		newSong.setSongID(playerLib.getSongID());
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Artist Name");
		
		// the try block is for error handing to catch the number format exception 
		try 
		{
			newSong.setArtistName(scan.next());

			System.out.println("Enter a name for your song");
			newSong.setSongName(scan.next());

			System.out.println("Enter Genre");
			newSong.setGenre(scan.next());

			Date relDate = new Date();
			
			System.out.println("Enter a Date - d: ");
			relDate.setDay(Integer.parseInt(scan.next()));
			
			System.out.println("Enter a Date - m: ");
			relDate.setMonth(Integer.parseInt(scan.next()));
			
			System.out.println("Enter a Date - yyyy: ");
			relDate.setYear(Integer.parseInt(scan.next()));
			
			newSong.setReleaseDate(relDate);	
			
			System.out.println("Date set: "+ relDate.getDate());
			
			System.out.println("Enter a duration for song in seconds:");
			int dur = scan.nextInt();
			newSong.setDuration(dur);
			
			System.out.println("Enter a BPM for song in seconds:");
			int bbpm = scan.nextInt();
			newSong.setBMP(bbpm);
		} 
		catch (NumberFormatException e)
		{
			
			// e.printStackTrace();
			System.out.println("Error in entry! Song was not added, try again.");
			displayMenu();
		}
		
		System.out.println("Song added to to the music library");
		System.out.println("The song ID is: " + newSong.getSongID());
		System.out.println("The song artist is: " + newSong.getArtistName());
		System.out.println("The song title is: " + newSong.getSongName());
		System.out.println("The song release date is: " + newSong.getReleaseDate().getDate());
		System.out.println("The BPM is: " + newSong.getBMP());
		System.out.println("The duration is: " + newSong.getDuration());
		
		addSongToLibrary(newSong);
		displayMenu();
		
		// increment the id by 1
		playerLib.setSongID(playerLib.getSongID() + 1);
	}
	
	public void displayAllLibrary(Library lib)
	{
		// get the array of songs from the Library class
		ArrayList<Song> allSongs = lib.getAllSongs();

		if (allSongs.isEmpty()) 
		{
			System.out.println("NOTHING IN THE LIBRARY\nADD MUSIC");
			displayMenu();
		}
		System.out.println("*************** Display All ****************");

		for (Song song : allSongs) 
		{
			System.out.println("The song ID is: " + song.getSongID());
			System.out.println("The song artist is: " + song.getArtistName());
			System.out.println("The song title is: " + song.getSongName());
			System.out.println("The song release date is: " + song.getReleaseDate().getDate());
			System.out.println("The BPM is: " + song.getBMP());
			System.out.println("The duration is: " + song.getDuration());
			System.out.println("====================================");			
		}
		
		System.out.println("********************************************");
		displayMenu();
	}

	public void displaySongInfo(Library lib, int id)
	{
		// get the array of songs from the Library class
		ArrayList<Song> allSongs = lib.getSongByID(id);

		if (allSongs.isEmpty()) 
		{
			System.out.println("Music Library is EMPTY\nADD MUSIC");
			displayMenu();
		}
		System.out.println("********** Display SONG Info *************");

		for (Song song : allSongs) 
		{
			System.out.println("The song ID is: " + song.getSongID());
			System.out.println("The song artist is: " + song.getArtistName());
			System.out.println("The song title is: " + song.getSongName());
			System.out.println("The song release date is: " + song.getReleaseDate().getDate());
			System.out.println("The BPM is: " + song.getBMP());
			System.out.println("The duration is: " + song.getDuration());
			System.out.println("====================================");			
		}
		
		System.out.println("********************************************");
		displayMenu();
	}

	public void displaySongInfo(Library lib, String songName)
	{
		// get the array of songs from the Library class
		ArrayList<Song> allSongs = lib.getSongByName(songName);

		if (allSongs.isEmpty()) 
		{
			System.out.println("Music Library is EMPTY\nADD MUSIC");
			displayMenu();
		}
		System.out.println("********** Display SONG Info *************");

		for (Song song : allSongs) 
		{
			System.out.println("The song ID is: " + song.getSongID());
			System.out.println("The song artist is: " + song.getArtistName());
			System.out.println("The song title is: " + song.getSongName());
			System.out.println("The song release date is: " + song.getReleaseDate().getDate());
			System.out.println("The BPM is: " + song.getBMP());
			System.out.println("The duration is: " + song.getDuration());
			System.out.println("====================================");			
		}
		
		System.out.println("********************************************");
		displayMenu();
	}
	
	private void addSongToLibrary(Song songToAdd)
	{
		if(songToAdd.equals(null))
			System.out.println("Cannot add a null song to the library");
		else
			playerLib.addSong(songToAdd); System.out.println("Added successfully");
	}
	
	
	public static void main(String []args) 
	{
		new FabMusicPlayer();
	}

}
