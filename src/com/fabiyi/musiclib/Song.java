package com.fabiyi.musiclib;

import com.fabiyi.tools.Date;
import com.fabiyi.tools.Music;

public class Song implements Music
{
	private String songName, artistName, genre;
	private Date releaseDate;
	private int duration, songID, bpm;
	/**
	 * @param songName
	 * @param artistName
	 * @param genre
	 * @param releaseDate
	 * @param duration
	 */
	public Song(String songName, String artistName, String genre, Date releaseDate, int duration)
	{
		this.songName = songName;
		this.artistName = artistName;
		this.genre = genre;
		this.releaseDate = releaseDate;
		this.duration = duration;
		songID++;
	}
	
	public Song() 
	{
		// TODO Auto-generated constructor stub
	}

	public String getSongName() {
		return songName;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	
	public int getSongID()
	{
		return songID;
	}
	
	public void setSongID(int songID)
	{
		this.songID = songID;
	}

	@Override
	public int getBMP() {
		// TODO Auto-generated method stub
		return bmp;
	}

	@Override
	public void setBMP(int bmp) {
		// TODO Auto-generated method stub
		this.bpm = bmp;
	}

	@Override
	public int getDuration() {
		// TODO Auto-generated method stub
		return this.duration;
	}

	@Override
	public void setDuration(int duration) {
		// TODO Auto-generated method stub
		this.duration = duration;
	}
	
}

