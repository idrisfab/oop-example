/**
 * 
 */
package com.fabiyi.musiclib;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author mrfab
 * The purpose of the library class is to hold several songs. It's going to be controlled using a lists
 * or collection.
 */
public class Library 
{
	// create an array list that will store all songs in it.
	private static ArrayList<Song> myLibrary = new ArrayList<Song>(); 
	private static int songIDInt=0;
	
	public Library() 
	{
		
	}
	
	public void setSongID(int newVal)
	{
		songIDInt = newVal; 		
	}
	
	public int getSongID()
	{
		return songIDInt;
	}
	
	public void addSong(Song mySong)
	{		
		// add 1 to the value of songID
		myLibrary.add(songIDInt++, mySong);
	}
	
	/**
	 * This will return all the song names that are in the library
	 * @param name
	 * @return ArrayList
	 */
	public ArrayList getSongByName(String name)
	{
		ArrayList tempList = new ArrayList<>();
		/* to find a song, we will iterate through the array list getting each song and checking the name to 
		see if it matches the one we are looking for */
		for (Iterator iterator = myLibrary.iterator(); iterator.hasNext();) 
		{
			Song song = (Song) iterator.next();
			
			if (song.getSongName() == name)
			{
				// if the name passed is the same as the current list then add it to the temp list
				tempList.add(name);
			}
		}
		
		// give the ArrayList back to the method.
		return tempList;
	}
	
	public ArrayList getSongByID(int songID)
	{
		ArrayList<Song> tempList = new ArrayList<Song>();
		/* to find a song, we will iterate through the array list getting each song and checking the name to 
		see if it matches the one we are looking for */
		for (Iterator iterator = myLibrary.iterator(); iterator.hasNext();) 
		{
			Song song = (Song) iterator.next();
			
			if (song.getSongID() == songID)
			{
				// retrieve the song at the ID position and put it in our temporary arraylist
				tempList.add(song);
				
				System.out.println("Song added from the library");
			}
		}
		
		// give the ArrayList back to the method.
		return tempList;
	}
	
	public ArrayList getAllSongs()
	{
		return myLibrary;
	}

	public void removeSongbyID(int songToRemoveID)
	{
		// remove the song from the array at the selected point.
		myLibrary.remove(songToRemoveID);
	}
	
}
