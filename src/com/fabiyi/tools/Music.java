/**
 * 
 */
package com.fabiyi.tools;

/**
 * @author idris
 *
 */
public interface Music 
{
	int bmp=0;
	int duration=0;
	
	/**
	 * set the duration
	 * */ 
	public int getDuration();
	
	/**
	 *  get the bmp of the track
	 * @return
	 */
	public int getBMP();
	
	/**
	 *  set the duration for the track in seconds
	 */
	public void setDuration(int duration);
	
	/**
	 *  set the bmp for the music
	 */
	public void setBMP(int bmp);
	
	
	
}
